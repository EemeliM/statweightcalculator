﻿using Microsoft.AspNetCore.Mvc.Rendering;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DPSParser
{
	public static class Parser
	{
		public static List<Outputs> Simulate(Inputs inputs, bool ignoreMaxMin = false)
		{
			var allOutputs = new List<Outputs>();

			// Todo DS/RUIN spec with +15% stamina
			// Base stats + Item stats + Chest enchant
			var stamina = inputs.BaseStats.Stamina + inputs.SelectedItems.Sum(x => x.Stamina) + 3;
		
			// Base stats + Item stats + Bracer enchant + Chest enchant + Gnome racial
			var intellect = (inputs.BaseStats.Intellect + inputs.SelectedItems.Sum(x => x.Intellect) + 10) * 1.05m;

			var critChance = (intellect / 4000m) + (inputs.SelectedItems.Sum(x => x.SpellCritical) / 100m); // 40 int ~ 1% crit
			var hitChance = 0.83m + (inputs.SelectedItems.Sum(x => x.SpellHit) / 100m);

			// Equipped item stats + spellpower enchant
			var spellpower = inputs.SelectedItems.Sum(x => x.SpellDamage) + 30;

			// Extra sauce
			spellpower += inputs.ExtraSP;

			var rand = new Random(DateTime.Now.Millisecond);

			var hundrethsCounter = inputs.NumberOfRepeats / 10;
			var currentCount = 0;
			var logCount = 0;

			foreach (var repeat in Enumerable.Range(0, inputs.NumberOfRepeats))
			{
				currentCount++;
				if (currentCount == hundrethsCounter)
				{
					logCount++;
					if (!ignoreMaxMin) Console.WriteLine($"{logCount}0%");
					currentCount = 0;
				}
				var output = new Outputs();
				var startTime = DateTime.Now;
				var currentTime = startTime;
				var endTime = startTime.AddSeconds(inputs.FightDuration);

				var currentHealth = stamina * 10;
				var currentMana = intellect * 20; // 1 int ~ 20 mana

				var shadowBoltCrits = 0m;
				output.TotalShadowboltHits = 0m;
				output.TotalShadowboltHitsWithImprovedShadowbolt = 0m;
				output.TotalShadowboltPartialResists = 0m;
				output.TotalShadowboltFullResists = 0m;

				var improvedShadowboltStacks = 0;
				var improvedShadowboltExpires = DateTime.MinValue;

				while (currentTime <= endTime)
				{
					//Console.WriteLine((currentTime - startTime).TotalSeconds);
					// Cast shadowbolt
					if (currentMana >= inputs.SpellCasts.ShadowboltManaCost)
					{
						currentTime = currentTime.AddMilliseconds(inputs.SpellCasts.ShadowboltCastTime);
						output.TotalShadowboltCasts++;

						currentMana -= inputs.SpellCasts.ShadowboltManaCost;

						// Base
						var damage = (decimal)rand.Next(455, 508);

						// Spell damage DO THIS BEFORE % BOOSTS!
						damage += spellpower;

						// Buffs and debuffs
						if (inputs.Debuffs.CurseOfShadow)
						{
							damage = damage * 1.1m;
						}
						if (inputs.Debuffs.ShadowWeaving)
						{
							damage = damage * 1.15m;
						}

						if (improvedShadowboltStacks > 0 && improvedShadowboltExpires > currentTime)
						{
							damage = damage * 1.2m;
							improvedShadowboltStacks--;
							output.TotalShadowboltHitsWithImprovedShadowbolt++;
						}

						// Crit or normal
						var critAsInt = (int)(critChance * 10000m);
						var critProc = rand.Next(0, 10001);
						// Crit
						if (critAsInt > critProc)
						{
							damage = damage * 2m;
							shadowBoltCrits++;

							// This is scuffed since the debuff starts after sb travel time which could be anything..?
							improvedShadowboltStacks = 4;
							improvedShadowboltExpires = currentTime.AddSeconds(11);
						}

						// Hit or miss
						var hitAsInt = (int)(hitChance * 10000m);
						var hitProc = rand.Next(0, 10001);
						// Hit
						if (hitAsInt > hitProc)
						{
							output.TotalShadowboltHits++;
						}
						// Miss
						else
						{
							// TODO partial resist properly
							var fullResist = rand.Next(2);
							if (fullResist == 0)
							{
								damage = 0m;
								output.TotalShadowboltFullResists++;
							}
							else
							{
								damage = damage / 2m;
								output.TotalShadowboltPartialResists++;
							}
						}

						output.TotalDamage += damage; 
						output.HighestSingleHit = Math.Max(output.HighestSingleHit, damage);
					}
					// Cast life tap
					else if (currentHealth > inputs.SpellCasts.LifetapHealthCost)
					{
						currentTime = currentTime.AddMilliseconds(inputs.SpellCasts.GlobalCooldown);

						if (!inputs.ActiveHealers)
						{
							currentHealth -= inputs.SpellCasts.LifetapHealthCost;
						}

						currentMana += inputs.SpellCasts.LifetapManaGain;
						output.TotalLifetaps++;
					}
					// Afk half a second
					else
					{
						currentTime = currentTime.AddMilliseconds(500);
						// TODO add wanding
						// TODO add five second rule
					}
				}
				output.DamagePerSecond = inputs.FightDuration != 0 ? output.TotalDamage / inputs.FightDuration : 0m;
				output.HitPercentage = output.TotalShadowboltCasts != 0 ? (output.TotalShadowboltHits / output.TotalShadowboltCasts) * 100m : 0m;
				output.CritPercentage = output.TotalShadowboltHits != 0 ? (shadowBoltCrits / output.TotalShadowboltHits) * 100m : 0m;

				allOutputs.Add(output);
			}

			var average = new Outputs()
			{
				TotalDamage = decimal.Round(allOutputs.Average(x => x.TotalDamage), 2),
				DamagePerSecond = decimal.Round(allOutputs.Average(x => x.DamagePerSecond), 2),
				CritPercentage = decimal.Round(allOutputs.Average(x => x.CritPercentage), 2),
				HighestSingleHit = decimal.Round(allOutputs.Average(x => x.HighestSingleHit), 2),
				HitPercentage = decimal.Round(allOutputs.Average(x => x.HitPercentage), 2),
				TotalLifetaps = decimal.Round(allOutputs.Average(x => x.TotalLifetaps), 2),
				TotalShadowboltCasts = decimal.Round(allOutputs.Average(x => x.TotalShadowboltCasts), 2),
				TotalShadowboltHits = decimal.Round(allOutputs.Average(x => x.TotalShadowboltHits), 2),
				TotalShadowboltHitsWithImprovedShadowbolt = decimal.Round(allOutputs.Average(x => x.TotalShadowboltHitsWithImprovedShadowbolt), 2),
				TotalShadowboltFullResists = decimal.Round(allOutputs.Average(x => x.TotalShadowboltFullResists), 2),
				TotalShadowboltPartialResists = decimal.Round(allOutputs.Average(x => x.TotalShadowboltPartialResists), 2)
			};

			if (ignoreMaxMin) return new List<Outputs>() { average };

			var max = new Outputs()
			{
				TotalDamage = decimal.Round(allOutputs.Max(x => x.TotalDamage), 2),
				DamagePerSecond = decimal.Round(allOutputs.Max(x => x.DamagePerSecond), 2),
				CritPercentage = decimal.Round(allOutputs.Max(x => x.CritPercentage), 2),
				HighestSingleHit = decimal.Round(allOutputs.Max(x => x.HighestSingleHit), 2),
				HitPercentage = decimal.Round(allOutputs.Max(x => x.HitPercentage), 2),
				TotalLifetaps = decimal.Round(allOutputs.Max(x => x.TotalLifetaps), 2),
				TotalShadowboltCasts = decimal.Round(allOutputs.Max(x => x.TotalShadowboltCasts), 2),
				TotalShadowboltHits = decimal.Round(allOutputs.Max(x => x.TotalShadowboltHits), 2),
				TotalShadowboltHitsWithImprovedShadowbolt = decimal.Round(allOutputs.Max(x => x.TotalShadowboltHitsWithImprovedShadowbolt), 2),
				TotalShadowboltFullResists = decimal.Round(allOutputs.Max(x => x.TotalShadowboltFullResists), 2),
				TotalShadowboltPartialResists = decimal.Round(allOutputs.Max(x => x.TotalShadowboltPartialResists), 2)
			};

			var min = new Outputs()
			{
				TotalDamage = decimal.Round(allOutputs.Min(x => x.TotalDamage), 2),
				DamagePerSecond = decimal.Round(allOutputs.Min(x => x.DamagePerSecond), 2),
				CritPercentage = decimal.Round(allOutputs.Min(x => x.CritPercentage), 2),
				HighestSingleHit = decimal.Round(allOutputs.Min(x => x.HighestSingleHit), 2),
				HitPercentage = decimal.Round(allOutputs.Min(x => x.HitPercentage), 2),
				TotalLifetaps = decimal.Round(allOutputs.Min(x => x.TotalLifetaps), 2),
				TotalShadowboltCasts = decimal.Round(allOutputs.Min(x => x.TotalShadowboltCasts), 2),
				TotalShadowboltHits = decimal.Round(allOutputs.Min(x => x.TotalShadowboltHits), 2),
				TotalShadowboltHitsWithImprovedShadowbolt = decimal.Round(allOutputs.Min(x => x.TotalShadowboltHitsWithImprovedShadowbolt), 2),
				TotalShadowboltFullResists = decimal.Round(allOutputs.Min(x => x.TotalShadowboltFullResists), 2),
				TotalShadowboltPartialResists = decimal.Round(allOutputs.Min(x => x.TotalShadowboltPartialResists), 2)
			};



			return new List<Outputs>() { average, max, min };
		}

		public static List<Item> GetAllItems()
		{
			var items = new List<Item>();
			var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\files\\Gear.xlsx");

			using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(path)))
			{
				var sheet = xlPackage.Workbook.Worksheets.FirstOrDefault();
				var totalRows = sheet.Dimension.End.Row;
				var totalColumns = sheet.Dimension.End.Column;

				foreach (var row in Enumerable.Range(2, totalRows))
				{
					var rowData = sheet.Cells[row, 1, row, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString()).ToArray();
					if (rowData.Length != 11) // Remember to edit this when adding new columns!!
					{
						continue;
					}
					var itemRow = new Item()
					{
						Type = (ItemType)Enum.Parse(typeof(ItemType), rowData[0]),
						Name = rowData[1],
						Stamina = decimal.Parse(rowData[2]),
						Intellect = decimal.Parse(rowData[3]),
						SpellDamage = decimal.Parse(rowData[4]),
						SpellCritical = decimal.Parse(rowData[5]),
						SpellHit = decimal.Parse(rowData[6]),
						Hp5 = decimal.Parse(rowData[7]),
						Mp5 = decimal.Parse(rowData[8]),
						Tierset = int.Parse(rowData[9]),
						Default = int.Parse(rowData[10])
					};
					items.Add(itemRow);
				}
			}

			return items;
		}
	}

	public class Outputs
	{
		public decimal TotalDamage { get; set; }
		public decimal DamagePerSecond { get; set; }
		public decimal HighestSingleHit { get; set; }
		public decimal HitPercentage { get; set; }
		public decimal CritPercentage { get; set; }
		public decimal TotalShadowboltCasts { get; set; }
		public decimal TotalShadowboltHits { get; set; }
		public decimal TotalShadowboltHitsWithImprovedShadowbolt { get; set; }
		public decimal TotalShadowboltPartialResists { get; set; }
		public decimal TotalShadowboltFullResists { get; set; }
		public decimal TotalLifetaps { get; set; }
	}

	public class Inputs
	{
		public int NumberOfRepeats { get; set; }
		public int FightDuration { get; set; }
		public bool ActiveHealers { get; set; }
		public BaseStats BaseStats { get; set; }
		public List<Item> SelectedItems { get; set; }
		public SpellCasts SpellCasts { get; set; }
		public Debuffs Debuffs { get; set; }
		public Gear InputsForGear { get; set; }
		public decimal ExtraSP { get; set; }

		public Inputs()
		{
			BaseStats = new BaseStats();
			SelectedItems = new List<Item>();
			SpellCasts = new SpellCasts();
			Debuffs = new Debuffs();
			InputsForGear = new Gear();
			ExtraSP = 0m;
		}
	}
	
	public class BaseStats
	{
		public decimal Stamina { get; set; }
		public decimal Intellect { get; set; }
		public decimal Spirit { get; set; }
		public decimal Hp5 { get; set; }
		public decimal Mp5 { get; set; }
	}

	public class ImprovedShadowbolt
	{
		public DateTime Expires { get; set; }
		public int Stacks { get; set; }
	}

	public class Gear
	{
		public string Head { get; set; }
		public List<SelectListItem> HeadOptions { get; set; }
		public string Neck { get; set; }
		public List<SelectListItem> NeckOptions { get; set; }
		public string Shoulder { get; set; }
		public List<SelectListItem> ShoulderOptions { get; set; }
		public string Back { get; set; }
		public List<SelectListItem> BackOptions { get; set; }
		public string Chest { get; set; }
		public List<SelectListItem> ChestOptions { get; set; }
		public string Wrist { get; set; }
		public List<SelectListItem> WristOptions { get; set; }
		public string Hands { get; set; }
		public List<SelectListItem> HandsOptions { get; set; }
		public string Waist { get; set; }
		public List<SelectListItem> WaistOptions { get; set; }
		public string Legs { get; set; }
		public List<SelectListItem> LegsOptions { get; set; }
		public string Feet { get; set; }
		public List<SelectListItem> FeetOptions { get; set; }
		public string Ring1 { get; set; }
		public string Ring2 { get; set; }
		public List<SelectListItem> Ring1Options { get; set; }
		public List<SelectListItem> Ring2Options { get; set; }
		public string Trinket1 { get; set; }
		public string Trinket2 { get; set; }
		public List<SelectListItem> Trinket1Options { get; set; }
		public List<SelectListItem> Trinket2Options { get; set; }
		public string Mainhand { get; set; }
		public List<SelectListItem> MainhandOptions { get; set; }
		public string Offhand { get; set; }
		public List<SelectListItem> OffhandOptions { get; set; }
		public string Wand { get; set; }
		public List<SelectListItem> WandOptions { get; set; }
	}

	public class Item
	{
		public ItemType Type { get; set; }
		public string Name { get; set; }
		public decimal Stamina { get; set; }
		public decimal Intellect { get; set; }
		public decimal SpellDamage { get; set; }
		public decimal SpellCritical { get; set; }
		public decimal SpellHit { get; set; }
		public decimal Hp5 { get; set; }
		public decimal Mp5 { get; set; }
		public int Tierset { get; set; }
		public int Default { get; set; }
	}

	public enum ItemType
	{
		Head,
		Neck,
		Shoulder,
		Back,
		Chest,
		Wrist,
		Hands,
		Waist,
		Legs,
		Feet,
		Ring,
		Trinket,
		Mainhand,
		Offhand,
		Wand,
		Set
	}

	public class SelectionCache
	{
		public int FightDuration { get; set; }
		public int Simulations { get; set; }
		public bool CurseOfShadow { get; set; }
		public bool ShadowWeaving { get; set; }
		public bool HealersEnabled { get; set; }
		public string Head { get; set; }
		public string Neck { get; set; }
		public string Shoulder { get; set; }
		public string Back { get; set; }
		public string Chest { get; set; }
		public string Wrist { get; set; }
		public string Hands { get; set; }
		public string Waist { get; set; }
		public string Legs { get; set; }
		public string Feet { get; set; }
		public string Ring1 { get; set; }
		public string Ring2 { get; set; }
		public string Trinket1 { get; set; }
		public string Trinket2 { get; set; }
		public string Mainhand { get; set; }
		public string Offhand { get; set; }
		public string Wand { get; set; }
	}

	public class SpellCasts
	{
		public double ShadowboltCastTime { get; set; }
		public decimal ShadowboltManaCost { get; set; }
		public decimal ShadowboltBaseDamage{ get; set; }
		public decimal LifetapHealthCost { get; set; }
		public decimal LifetapManaGain { get; set; }
		public double GlobalCooldown { get; set; }
	}
	public class Debuffs
	{
		public bool CurseOfShadow { get; set; }
		public bool ShadowWeaving { get; set; }
	}

	public class ResultViewModel
	{
		public List<Outputs> Outputs { get; set; }
		public int CritWeight { get; set; }
		public int HitWeight { get; set; }
	}
}
