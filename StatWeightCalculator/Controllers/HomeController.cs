﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DPSParser;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using StatWeightCalculator.Models;

namespace StatWeightCalculator.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			return RedirectToAction("Simulate");

			var talents = new Talents();
			talents.FillSMRuin();
			return View(talents);
		}

		public IActionResult Simulate()
		{
			var data = new Inputs();
			var allItems = Parser.GetAllItems();

			SelectionCache cache = null;

			if (TempData.ContainsKey("selections"))
			{
				cache = TempData.Get<SelectionCache>("selections");
			}

			// Populate item selectors
			data.InputsForGear.HeadOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Head)) { data.InputsForGear.HeadOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Head == item.Name : item.Default == 1 }); }
			data.InputsForGear.NeckOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Neck)) { data.InputsForGear.NeckOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Neck == item.Name : item.Default == 1 }); }
			data.InputsForGear.ShoulderOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Shoulder)) { data.InputsForGear.ShoulderOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Shoulder == item.Name : item.Default == 1 }); }
			data.InputsForGear.BackOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Back)) { data.InputsForGear.BackOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Back == item.Name : item.Default == 1 }); }
			data.InputsForGear.ChestOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Chest)) { data.InputsForGear.ChestOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Chest == item.Name : item.Default == 1 }); }
			data.InputsForGear.WristOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Wrist)) { data.InputsForGear.WristOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Wrist == item.Name : item.Default == 1 }); }
			data.InputsForGear.HandsOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Hands)) { data.InputsForGear.HandsOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Hands == item.Name : item.Default == 1 }); }
			data.InputsForGear.WaistOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Waist)) { data.InputsForGear.WaistOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Waist == item.Name : item.Default == 1 }); }
			data.InputsForGear.LegsOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Legs)) { data.InputsForGear.LegsOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Legs == item.Name : item.Default == 1 }); }
			data.InputsForGear.FeetOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Feet)) { data.InputsForGear.FeetOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Feet == item.Name : item.Default == 1 }); }
			data.InputsForGear.Ring1Options = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Ring)) { data.InputsForGear.Ring1Options.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Ring1 == item.Name : item.Default == 1 }); }
			data.InputsForGear.Ring2Options = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Ring)) { data.InputsForGear.Ring2Options.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Ring2 == item.Name : item.Default == 2 }); }
			data.InputsForGear.Trinket1Options = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Trinket)) { data.InputsForGear.Trinket1Options.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Trinket1 == item.Name : item.Default == 1 }); }
			data.InputsForGear.Trinket2Options = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Trinket)) { data.InputsForGear.Trinket2Options.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Trinket2 == item.Name : item.Default == 2 }); }
			data.InputsForGear.MainhandOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Mainhand)) { data.InputsForGear.MainhandOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Mainhand == item.Name : item.Default == 1 }); }
			data.InputsForGear.OffhandOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Offhand)) { data.InputsForGear.OffhandOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Offhand == item.Name : item.Default == 1 }); }
			data.InputsForGear.WandOptions = new List<SelectListItem>();
			foreach (var item in allItems.Where(x => x.Type == ItemType.Wand)) { data.InputsForGear.WandOptions.Add(new SelectListItem() { Value = item.Name, Text = item.Name, Selected = cache != null ? cache.Wand == item.Name : item.Default == 1 }); }

			if (cache != null)
			{
				data.ActiveHealers = cache.HealersEnabled;
				data.FightDuration = cache.FightDuration;
				data.NumberOfRepeats = cache.Simulations;
				data.Debuffs.ShadowWeaving = cache.ShadowWeaving;
				data.Debuffs.CurseOfShadow = cache.CurseOfShadow;
			}
			else
			{
				data.ActiveHealers = true;
				data.FightDuration = 180;
				data.NumberOfRepeats = 10000;
				data.Debuffs.ShadowWeaving = true;
				data.Debuffs.CurseOfShadow = true;
			}

			return View(data);
		}

		public IActionResult Results()
		{
			var data = new ResultViewModel();
			if (TempData.ContainsKey("data"))
			{
				data = TempData.Get<ResultViewModel>("data");
			}
			return View(data);
		}

		[HttpPost]
		public IActionResult RunSimulation(Inputs inputs)
		{
			inputs.NumberOfRepeats = Math.Min(inputs.NumberOfRepeats, 10000);

			inputs.BaseStats.Stamina = 64;
			inputs.BaseStats.Intellect = 118;
			inputs.BaseStats.Spirit = 115;

			// TODO: Add these to fight duration
			inputs.BaseStats.Hp5 = 0;
			inputs.BaseStats.Mp5 = 0;

			inputs.SpellCasts.GlobalCooldown = 1000;
			inputs.SpellCasts.LifetapHealthCost = 697;
			inputs.SpellCasts.LifetapManaGain = 837;
			inputs.SpellCasts.ShadowboltCastTime = 2500;
			inputs.SpellCasts.ShadowboltManaCost = 370;

			var allItems = Parser.GetAllItems();

			// Equip selected gear
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Head));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Neck));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Shoulder));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Back));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Chest));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Wrist));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Hands));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Waist));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Legs));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Feet));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Ring1));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Ring2));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Trinket1));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Trinket2));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Mainhand));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Offhand));
			inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == inputs.InputsForGear.Wand));

			// Check for tier 2 3-set bonus
			if (inputs.SelectedItems.Where(x => x.Tierset == 2).Count() >= 3)
			{
				inputs.SelectedItems.Add(allItems.FirstOrDefault(x => x.Name == "Nemesis"));
			}

			// Cache selections
			var cache = new SelectionCache()
			{
				FightDuration = inputs.FightDuration,
				Simulations = inputs.NumberOfRepeats,
				HealersEnabled = inputs.ActiveHealers,
				CurseOfShadow = inputs.Debuffs.CurseOfShadow,
				ShadowWeaving = inputs.Debuffs.ShadowWeaving,

				Head = inputs.InputsForGear.Head,
				Neck = inputs.InputsForGear.Neck,
				Shoulder = inputs.InputsForGear.Shoulder,
				Back = inputs.InputsForGear.Back,
				Chest = inputs.InputsForGear.Chest,
				Wrist = inputs.InputsForGear.Wrist,
				Waist = inputs.InputsForGear.Waist,
				Hands = inputs.InputsForGear.Hands,
				Legs = inputs.InputsForGear.Legs,
				Feet = inputs.InputsForGear.Feet,
				Ring1 = inputs.InputsForGear.Ring1,
				Ring2 = inputs.InputsForGear.Ring2,
				Trinket1 = inputs.InputsForGear.Trinket1,
				Trinket2 = inputs.InputsForGear.Trinket2,
				Mainhand = inputs.InputsForGear.Mainhand,
				Offhand = inputs.InputsForGear.Offhand,
				Wand = inputs.InputsForGear.Wand
			};
			TempData.Put("selections", cache);

			var intialValues = Parser.Simulate(inputs);

			var closeEnoughForCrit = false;
			var closeEnoughForHit = false;

			// Go a bit faster
			inputs.NumberOfRepeats = 10000;

			// Check for crit first
			inputs.SelectedItems.FirstOrDefault().SpellCritical++;
			var plusOneCritDps = Parser.Simulate(inputs, true).FirstOrDefault().DamagePerSecond;
			inputs.SelectedItems.FirstOrDefault().SpellCritical--;

			// Check for hit
			inputs.SelectedItems.FirstOrDefault().SpellHit++;
			var plusOneHitDps = Parser.Simulate(inputs, true).FirstOrDefault().DamagePerSecond;
			inputs.SelectedItems.FirstOrDefault().SpellHit--;

			// Add sp to get same dps as +1 crit and +1hit
			// start at +5 sp
			var spToCrit = 5;
			var spToHit = 5;

			inputs.ExtraSP += 5;
			var newDps = Parser.Simulate(inputs, true).FirstOrDefault().DamagePerSecond;
			while (true)
			{
				if (newDps >= plusOneCritDps)
				{
					closeEnoughForCrit = true;
				}
				if (newDps >= plusOneHitDps)
				{
					closeEnoughForHit = true;
				}

				if (closeEnoughForCrit && closeEnoughForHit) break;

				if (!closeEnoughForCrit) spToCrit++;
				if (!closeEnoughForHit) spToHit++;
				inputs.ExtraSP++;

				newDps = Parser.Simulate(inputs, true).FirstOrDefault().DamagePerSecond;
			}

			Console.WriteLine($"1 crit = {spToCrit}sp & 1 hit = {spToHit}sp");

			var results = new ResultViewModel() { Outputs = intialValues, CritWeight = spToCrit, HitWeight = spToHit };

			TempData.Put("data", results);
			return RedirectToAction("Results");
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
	public static class TempDataExtensions
	{
		public static void Put<T>(this ITempDataDictionary tempData, string key, T value) where T : class
		{
			tempData[key] = JsonConvert.SerializeObject(value);
		}

		public static T Get<T>(this ITempDataDictionary tempData, string key) where T : class
		{
			object o;
			tempData.TryGetValue(key, out o);
			return o == null ? null : JsonConvert.DeserializeObject<T>((string)o);
		}
	}
}
