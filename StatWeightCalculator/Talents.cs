﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StatWeightCalculator
{
	public class Talents
	{
		public List<TalentTree> Trees { get; set; }

		public Talents()
		{
			// Intit trees
			Trees = new List<TalentTree>();
			var affliction = new TalentTree() { Name = "Affliction", TreeTalents = new List<Talent>() };

			affliction.TreeTalents.Add(new Talent() { Name = "Suppression",					 MaxPoints = 5, Level = 0, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Improved Corruption",			 MaxPoints = 5, Level = 0, CurrentPoints = 0 });

			affliction.TreeTalents.Add(new Talent() { Name = "Improved Curse of Agony",	     MaxPoints = 3, Level = 1, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Improved Drain Soul",          MaxPoints = 2, Level = 1, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Improved Life Tap",            MaxPoints = 2, Level = 1, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Improved Drain Life",          MaxPoints = 5, Level = 1, CurrentPoints = 0 });

			affliction.TreeTalents.Add(new Talent() { Name = "Improved Curse of Agony",      MaxPoints = 3, Level = 2, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Fel Concentration",            MaxPoints = 5, Level = 2, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Amplify Curse",                MaxPoints = 1, Level = 2, CurrentPoints = 0 });

			affliction.TreeTalents.Add(new Talent() { Name = "Grim Reach",                   MaxPoints = 2, Level = 3, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Nightfall",                    MaxPoints = 2, Level = 3, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Improved Drain Mana",          MaxPoints = 2, Level = 3, CurrentPoints = 0 });

			affliction.TreeTalents.Add(new Talent() { Name = "Siphon Life",                  MaxPoints = 1, Level = 4, CurrentPoints = 0 });
			affliction.TreeTalents.Add(new Talent() { Name = "Curse of Exhaustion",          MaxPoints = 1, Level = 4, CurrentPoints = 0, Requires = "Amplify Curse" });
			affliction.TreeTalents.Add(new Talent() { Name = "Improved Curse of Exhaustion", MaxPoints = 4, Level = 4, CurrentPoints = 0, Requires = "Curse of Exhaustion" });

			affliction.TreeTalents.Add(new Talent() { Name = "Shadow Mastery",               MaxPoints = 5, Level = 5, CurrentPoints = 0, Requires = "Siphon Life" });
	
			affliction.TreeTalents.Add(new Talent() { Name = "Dark Pact",                    MaxPoints = 1, Level = 6, CurrentPoints = 0 });

			Trees.Add(affliction);

			var demonology = new TalentTree() { Name = "Demonology", TreeTalents = new List<Talent>() };

			demonology.TreeTalents.Add(new Talent() { Name = "Improved Healthstone",   MaxPoints = 2, Level = 0, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Improved Imp",           MaxPoints = 3, Level = 0, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Demonic Embrace",        MaxPoints = 5, Level = 0, CurrentPoints = 0 });

			demonology.TreeTalents.Add(new Talent() { Name = "Improved Health Funnel", MaxPoints = 2, Level = 1, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Improved Voidwalker",    MaxPoints = 3, Level = 1, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Fel Intellect",          MaxPoints = 5, Level = 1, CurrentPoints = 0 });

			demonology.TreeTalents.Add(new Talent() { Name = "Improved Succubus",      MaxPoints = 3, Level = 2, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Fel Domination",         MaxPoints = 1, Level = 2, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Fel Stamina",            MaxPoints = 1, Level = 2, CurrentPoints = 0 });

			demonology.TreeTalents.Add(new Talent() { Name = "Master Summoner",        MaxPoints = 2, Level = 3, CurrentPoints = 0, Requires = "Fel Domination" });
			demonology.TreeTalents.Add(new Talent() { Name = "Unholy Power",           MaxPoints = 5, Level = 3, CurrentPoints = 0 });

			demonology.TreeTalents.Add(new Talent() { Name = "Improved Enslave Demon", MaxPoints = 5, Level = 4, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Demonic Sacrifice",      MaxPoints = 1, Level = 4, CurrentPoints = 0 });
			demonology.TreeTalents.Add(new Talent() { Name = "Improved Firestone",     MaxPoints = 2, Level = 4, CurrentPoints = 0 });

			demonology.TreeTalents.Add(new Talent() { Name = "Master Demonologist",    MaxPoints = 5, Level = 5, CurrentPoints = 0, Requires = "Unholy Power" });

			demonology.TreeTalents.Add(new Talent() { Name = "Soul Link",              MaxPoints = 1, Level = 6, CurrentPoints = 0, Requires = "Demonic Sacrifice" });
			demonology.TreeTalents.Add(new Talent() { Name = "Improved Spellstone",    MaxPoints = 2, Level = 6, CurrentPoints = 0 });

			Trees.Add(demonology);

			var destruction = new TalentTree() { Name = "Destruction", TreeTalents = new List<Talent>() };

			destruction.TreeTalents.Add(new Talent() { Name = "Improved Shadow Bolt",  MaxPoints = 5, Level = 0, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Cataclysm",             MaxPoints = 5, Level = 0, CurrentPoints = 0 });

			destruction.TreeTalents.Add(new Talent() { Name = "Bane",                  MaxPoints = 5, Level = 1, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Aftermath",             MaxPoints = 5, Level = 1, CurrentPoints = 0 });

			destruction.TreeTalents.Add(new Talent() { Name = "Improved Firebolt",     MaxPoints = 2, Level = 2, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Improved Lash of Pain", MaxPoints = 2, Level = 2, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Devastation",           MaxPoints = 5, Level = 2, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Shadowburn",            MaxPoints = 1, Level = 2, CurrentPoints = 0 });

			destruction.TreeTalents.Add(new Talent() { Name = "Intensity",             MaxPoints = 2, Level = 3, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Destructive Reach",     MaxPoints = 2, Level = 3, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Improved Searing Pain", MaxPoints = 5, Level = 3, CurrentPoints = 0 });

			destruction.TreeTalents.Add(new Talent() { Name = "Pyroclasm",             MaxPoints = 2, Level = 4, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Improved Immolate",     MaxPoints = 5, Level = 4, CurrentPoints = 0 });
			destruction.TreeTalents.Add(new Talent() { Name = "Ruin",                  MaxPoints = 1, Level = 4, CurrentPoints = 0, Requires = "Devastation" });

			destruction.TreeTalents.Add(new Talent() { Name = "Emberstorm",            MaxPoints = 5, Level = 5, CurrentPoints = 0 });

			destruction.TreeTalents.Add(new Talent() { Name = "Conflagrate",           MaxPoints = 1, Level = 6, CurrentPoints = 0, Requires = "Improved Immolate" });

			Trees.Add(destruction);
		}

		public void AddTalent(string Name, int amount = 1)
		{
			foreach (var times in Enumerable.Range(0, amount))
			{
				Console.WriteLine($"Adding { Name }");

				// Check if talent can be added
				var tree = Trees.First(x => x.DoesContainTalent(Name));
				var talent = tree.TreeTalents.First(x => x.Name == Name);

				if (talent.MaxPoints == talent.CurrentPoints)
				{
					Console.WriteLine("Talent is full");
					return;
				}

				var talentMinimumRequirement = talent.Level * 5;

				if (tree.TotalPoints < talentMinimumRequirement)
				{
					Console.WriteLine($"Not enough points in tree, requires { talentMinimumRequirement }, current is { tree.TotalPoints }");
					return;
				}

				// Check that possible requirement has been fullfilled
				if (talent.Requires != null)
				{
					var requiredTalent = tree.TreeTalents.First(x => x.Name == talent.Requires);
					if (requiredTalent.CurrentPoints != requiredTalent.MaxPoints)
					{
						Console.WriteLine($"Talent requires talent { requiredTalent.Name } to be full, currently is { requiredTalent.CurrentPoints }/{ requiredTalent.MaxPoints }");
						return;
					}
				}

				talent.CurrentPoints++;
			}
		}

		public void FillSMRuin()
		{
			AddTalent("Improved Corruption", 5);
			AddTalent("Improved Life Tap", 2);
			AddTalent("Improved Drain Life", 3);
			AddTalent("Amplify Curse");
			AddTalent("Fel Concentration", 4);
			AddTalent("Nightfall", 2);
			AddTalent("Grim Reach", 2);
			AddTalent("Fel Concentration");
			AddTalent("Siphon Life");
			AddTalent("Improved Drain Life", 2);
			AddTalent("Suppression", 2);
			AddTalent("Shadow Mastery", 5);
			AddTalent("Improved Shadow Bolt", 5);
			AddTalent("Bane", 5);
			AddTalent("Shadowburn");
			AddTalent("Devastation", 4);
			AddTalent("Destructive Reach", 2);
			AddTalent("Devastation");
			AddTalent("Improved Searing Pain", 2);
			AddTalent("Ruin");
		}

		public string SpentTalents(string name)
		{
			return Trees.FirstOrDefault(x => x.TreeTalents.Any(y => y.Name == name)).TreeTalents.FirstOrDefault(x => x.Name == name).CurrentPoints.ToString();
		}
	}

	public class TalentTree
	{
		public string Name { get; set; }
		public List<Talent> TreeTalents { get; set; }
		public int TotalPoints
		{
			get
			{
				return TreeTalents.Sum(x => x.CurrentPoints);
			}
		}
		public bool DoesContainTalent(string Name)
		{
			return TreeTalents.Any(x => x.Name == Name);
		}
	}

	public class Talent
	{
		public string Name { get; set; }
		public int Level { get; set; }
		public int MaxPoints { get; set; }
		public int CurrentPoints { get; set; }
		public string Requires { get; set; }
	}
}
